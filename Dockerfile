FROM python 

#installer des scripts
RUN pip install pandas
RUN pip install pytest

ENV PYTHONPATH "${PYTHONPATH}:/src"

#copier le code
COPY ./src /src

#jouer un code
CMD ["python", "/src/script.py"]